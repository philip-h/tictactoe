package com.phlash;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author philipjnr.habib
 */
public class TicTacToe extends JFrame implements ActionListener
{
    private static final long serialVersionUID = 1L;
    JButton[][] buttons = new JButton[3][3];
    JButton reset = new JButton("Reset");
    private boolean turn = true;
    int cats = 0;

    public TicTacToe()
    {
        super("Tic Tac Toe");
        setSize(new Dimension(400, 300));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        init();
        setVisible(true);
    }

    private void init()
    {
        JPanel borderPane = new JPanel(new BorderLayout());
        JPanel mainPane = new JPanel(new GridLayout(3, 3, 10, 10));
        mainPane.setBackground(Color.magenta);
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                buttons[i][j] = new JButton();
                buttons[i][j].setFont(new Font("Dialog", Font.PLAIN, 25));
                buttons[i][j].addActionListener(this);
                buttons[i][j].setBackground(Color.black);
                buttons[i][j].setForeground(Color.white);
                mainPane.add(buttons[i][j]);
            }
        }
        reset.addActionListener(this);
        borderPane.add(mainPane, BorderLayout.CENTER);
        borderPane.add(reset, BorderLayout.SOUTH);
        setContentPane(borderPane);
    }

    private void reset()
    {
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                buttons[i][j].setText("");
            }
        }
        cats = 0;
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        JButton buttonPressed = (JButton) e.getSource();
        if (buttonPressed.getText().equals("Reset"))
        {
            reset();
        } else if (buttonPressed.getText().equals(""))
        {
            if (turn)
            {
                buttonPressed.setText("X");
                cats++;
                turn = !turn;
                checkWin();
            } else
            {
                buttonPressed.setText("O");
                cats++;
                turn = !turn;
                checkWin();
            }
        }

    }

    private void checkWin()
    {
        for (int i = 0; i < 3; i++)
        {
            //Check Horizontal
            if (buttons[i][0].getText().equals(buttons[i][1].getText()) && buttons[i][1].getText().equals(buttons[i][2].getText()) && !buttons[i][0].getText().equals(""))
            {
                JOptionPane.showMessageDialog(null, "Congratz, " + buttons[i][0].getText() + "Wins!");
                reset();
            }
            //Check Horizontal
            if (buttons[0][i].getText().equals(buttons[1][i].getText()) && buttons[1][i].getText().equals(buttons[2][i].getText()) && !buttons[0][i].getText().equals(""))
            {
                JOptionPane.showMessageDialog(null, "Congratz, " + buttons[0][i].getText() + "Wins!");
                reset();
            }
        }

        //Check Diagonal down
        if (buttons[0][0].getText().equals(buttons[1][1].getText()) && buttons[1][1].getText().equals(buttons[2][2].getText()) && !buttons[0][0].getText().equals(""))
        {
            JOptionPane.showMessageDialog(null, "Congratz, " + buttons[0][0].getText() + "Wins!");
            reset();
        }

        //Check Diagonal up
        if (buttons[2][0].getText().equals(buttons[1][1].getText()) && buttons[1][1].getText().equals(buttons[0][2].getText()) && !buttons[2][0].getText().equals(""))
        {
            JOptionPane.showMessageDialog(null, "Congratz, " + buttons[2][0].getText() + "Wins!");
            reset();
        }
        if (cats > 8)
        {
            JOptionPane.showMessageDialog(null, "Tie Game!");
            reset();
        }

    }

    public static void main(String[] args)
    {
        new TicTacToe();
    }

}
